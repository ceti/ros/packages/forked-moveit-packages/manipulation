# MoveIt Manipulation Package

Forked juli 2020 and extended with adaption of velocity / acceleration.

Original repository: https://github.com/ros-planning/moveit/tree/master/moveit_ros/manipulation

## How to install it:

* navigate into the src-folder of your workspace
* create a folder with the name "moveit_ros"
* cd into this folder and create another one with the same name
* cd again into the newly created folder
* clone the package into that folder

## How to modify the velocity:

You can do this by the configuration of four parameters in the parameter-server of ROS:

* max_grasp_approach_velocity (max velocity from start-pose to pre-approach pose and starting from pre-place-approach pose)
* max_grasp_approach_acceleration (max acceleration from start-pose to pre-approach pose and starting from pre-place-approach pose)
* max_grasp_transition_velocity (max velocity from post-grasp pose to pre-place-approach pose)
* max_grasp_transition_acceleration (max acceleration from post-grasp pose to pre-place-approach pose)
